﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum EQualifyingRule
  {
    FillFromLeague,
    FillFromLeagueInOrder,
    FillFromCompTable,
    FillFromCompTableBackup,
    FillFromCompTableBackupLeague,
    FillFromLeagueMaxFromCountry,
    FillFromSpecialTeams,
    FillWithTeam,
    NoRule,
  }
}
