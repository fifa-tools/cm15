﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum EPlayingIntensity
  {
    Normal,
    Poor,
    Intense,
    UsePlayer,
  }
}
