﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum EPlayingDirection
  {
    Standing,
    Left,
    DiagonalLeft,
    Stright,
    DiagonalRight,
    Right,
  }
}
