﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum ECompressionMode
  {
    None,
    Compressed_10FB,
    Chunkzip,
    Chunkzip2,
    Chunkref,
    Chunkref2,
    EASF,
    Unknown,
  }
}
