﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public class StageList : IdArrayList
  {
    public StageList()
      : base(typeof (Stage))
    {
    }
  }
}
