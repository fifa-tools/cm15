﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("14.3.0.0")]
[assembly: AssemblyTitle("FifaLibrary")]
[assembly: Guid("cdf18050-888e-4b89-88ba-27c3f14a456a")]
[assembly: AssemblyDescription("Library for accessing FIFA files and data")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fifa Master")]
[assembly: AssemblyProduct("FifaLibrary")]
[assembly: AssemblyCopyright("Copyright © Fifa-Master")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("14.3.0.0")]
