﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum ERole
  {
    Goalkeeper,
    Sweeper,
    Right_Wing_Back,
    Right_Back,
    Right_Central_Back,
    Central_Back,
    Left_Central_Back,
    Left_Back,
    Left_Wing_Back,
    Right_Defensive_Midfielder,
    Central_Defensive_Midfielder,
    Left_Defensive_Midfielder,
    Right_Midfielder,
    Right_Central_Midfielder,
    Central_Midfielder,
    Left_Central_Midfielder,
    Left_Midfielder,
    Right_Advanced_Midfielder,
    Central_Advanced_Midfielder,
    Left_Advanced_Midfielder,
    Right_Forward,
    Central_Forward,
    Left_Forward,
    Right_Wing,
    Right_Striker,
    Central_Striker,
    Left_Striker,
    Left_Wing,
    Substitute,
    Tribune,
  }
}
