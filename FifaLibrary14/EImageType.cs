﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum EImageType
  {
    DXT1 = 0,
    DXT3 = 1,
    DXT5 = 2,
    A8R8G8B8 = 3,
    GREY8 = 4,
    GREY8ALFA8 = 5,
    A4R4G4B4 = 109, // 0x0000006D
    R5G6B5 = 120, // 0x00000078
    BIT8 = 123, // 0x0000007B
    X1R5G5B5 = 126, // 0x0000007E
    R8G8B8 = 127, // 0x0000007F
  }
}
