﻿// Original code created by Rinaldo

namespace FifaLibrary
{
  public enum EFifaObjects
  {
    FifaCountry = 1,
    FifaLeague = 2,
    FifaTeam = 4,
    FifaPlayer = 8,
    FifaStadium = 16, // 0x00000010
    FifaReferee = 32, // 0x00000020
    FifaTournament = 64, // 0x00000040
    FifaColor = 128, // 0x00000080
    FifaFormation = 256, // 0x00000100
    FifaSponsor = 512, // 0x00000200
    FifaRole = 1024, // 0x00000400
    FifaBall = 2048, // 0x00000800
    FifaAdboard = 4096, // 0x00001000
    FifaShoes = 8192, // 0x00002000
    FifaGrass = 16384, // 0x00004000
    FifaNet = 32768, // 0x00008000
    FifaJerseyFont = 65536, // 0x00010000
    FifaShortsFont = 131072, // 0x00020000
    FifaKit = 262144, // 0x00040000
    FifaReward = 524288, // 0x00080000
    FifaManager = 1048576, // 0x00100000
    FifaMarket = 2097152, // 0x00200000
    FifaFixtures = 4194304, // 0x00400000
    FifaMowingPattern = 8388608, // 0x00800000
    FifaWeather = 16777216, // 0x01000000
  }
}
